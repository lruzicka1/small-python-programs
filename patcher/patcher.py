#!/usr/bin/python

"""
Module Patcher
by Lukáš Růžička, lruzicka@redhat.com, 2021.

This code is published under the General Public License, version 3.

The Module Patcher is a simple script that iterates over a list of module files in a directory and
patches them with the following tags:

* Inserting the :_module-type: TYPE and :experimental: variables at the beginning of all module files in a directory 
(TYPE should be set according the file name, for example, module files starting with the proc_ prefix will include 
:_module-type: PROCEDURE in the header level).
* Inserting [role="_abstract"] before the first paragraph of a module or an assembly.
* Inserting [role="_additional-resources"] before .Additional resources or == Additional resources

If some of the tags are found at the beginning of the file, it will be skipped and will not be patched. 
Partially edited files will therefore NOT BE PATCHED in this version of the script. 
You should check manually, that the files have been correctly patched before using them in production.
"""    

import argparse
import re
import os


class Parser:
    """ The Parser class handles the CLI arguments. """
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('-d', '--dry', action='store_true', help='Run the script but do not write out anything or modify existing files.')
        self.parser.add_argument('-b', '--backup', action='store_true', help='Save the modified content in a backup file and keep the original one.')
        self.parser.add_argument('-l', '--location', default='.', help='Provide the target directory in which files should be modified.')

    def provide_arguments(self):
        args = self.parser.parse_args()
        return args

class Tree:
    """ The tree represents the working directory content. """
    def __init__(self, location):
        """ Scan the working directory. """
        self.wd_path = os.path.abspath(location)
        self.file_list = os.scandir(self.wd_path)
    
    def take_next(self):
        """ Return the next occurence in the file list and return it. 

        The "dir" value will be returned if the entry is a directory. If the list is empty, then None will be returned. """
        try:
            file_path = next(self.file_list)
            if ".py" in file_path.name:
                return "skip"
            elif file_path.is_file():
                return file_path.path
            else:
                return "skip"
        except StopIteration:
            return None

class Handler:
    """ This class handles the content of the file.  """
    def __init__(self, filename, backup=False, dry=False):
        """ Set up the variables and read the file content. """
        self.path = os.path.dirname(filename)
        self.basename = os.path.basename(filename)
        self.backup = backup
        self.dry = dry
        with open(filename, 'r') as ifile:
            self.file_content = ifile.readlines()

    def get_content(self):
        """ Return the file content. """
        return self.file_content

    def get_filetype(self):
        """ Set up the module type according to the file name. """
        marker = self.basename.split('_')[0]
        if marker == "proc":
            file_type = "PROCEDURE"
        elif marker == "ref":
            file_type = "REFERENCE"
        elif marker == "con":
            file_type = "CONCEPT"
        else:
            file_type = None
        return file_type

    def update_content(self, content):
        """ Update the content of the file. """
        self.file_content = content

    def save_content(self):
        """ Save the content of the file on the disk. """
        if self.backup:
            self.basename = f"{self.basename}.patched"
        path = os.path.join(self.path, self.basename)
        # Only write out file, if there is content and if --dry is not chosen.
        if self.file_content and self.dry == False:
            with open(path, 'w') as ofile:
                output = '\n'.join(self.file_content)
                ofile.write(output)
            print("..... File written.")
        else:
            print("..... Writing file skipped.")

class Patcher:
    """ This manipulates the content of the file. """
    def __init__(self, content, module_type):
        """ Set up variables. """
        self.original = content
        self.module_type = module_type
        #self.typeline = f":_module-type: {module_type}"
        #self.patched = [self.typeline, ":experimental:"]
        self.patched = []
        self.idfound = False
        self.hfound = False
        self.patch_test = []

    def test(self):
        for line in self.original:
            line = line.rstrip()
            if ":_module-type:" in line:
                self.patch_test.append("mdtype")
            if line == ":experimental:":
                self.patch_test.append("experimental")
            if '[role="_abstract"]' in line:
                self.patch_test.append("abstract")
            if '[role="_additional-resources"]' in line:
                self.patch_test.append("add-res")
        return self.patch_test

    def patch(self):
        """ Patch the file content with necessary changes. """
        if "mdtype" in self.patch_test:
            print("..... Tag :_module-type: already found in the file. Skipping.")
        else:
            print(f"..... Adding the {self.module_type} tag to the top of the file.")
            self.patched.append(f":_module-type: {self.module_type}")
        if "experimental" in self.patch_test:
            print("..... Tag :experimental: already found in the file. Skipping.")
        else:
            print("..... Adding the :experimental: tag to the top of the file.")
            self.patched.append(":experimental:")
        for line in self.original:
            line = line.rstrip()
            id_match = re.compile('\[id="\S+"\]').match(line)
            head_match = re.compile('=\s.+').match(line)
            add_match = re.compile('\.Additional resources|== Additional resources').match(line)
            if self.idfound == True and self.hfound == True and line != "":
                self.idfound = False
                self.hfound = False
                if 'abstract' in self.patch_test:
                    print('..... Tag [role="_abstract"] already found in the file. Skipping.')
                else:
                    self.patched.append('[role="_abstract"]')
                    print(f'..... Adding the [role="_abstract"] tag before the first paragraph.')
            elif id_match:
                self.idfound = True
            elif head_match:
                self.hfound = True
            elif add_match:
                if 'add-res' in self.patch_test:
                    print('..... Tag [role="_additional-resources"] already found in the file. Skipping.')
                else:    
                    self.patched.append('[role="_additional-resources"]')
                    print(f'..... Found section Additional resources, adding the [role="_additional-resources"] tag.')
            self.patched.append(line)
        print("..... File patched.")
        # Append one empty line at the end of each module to not break asciidoc rules.
        self.patched.append("  ")
        return self.patched

def main():
    # Read the CLI arguments.
    cli_parser = Parser()
    cli = cli_parser.provide_arguments()
    # Read the content of the working directory.
    file_tree = Tree(cli.location)
    # Iterate over the file tree and patch all files.
    while True:
        processed_path = file_tree.take_next()
        if processed_path and processed_path != "skip":
            print(f"Patching {processed_path}")
            # Read the file into the Handler
            processed_file = Handler(processed_path, backup=cli.backup, dry=cli.dry)
            # Get the file content and the file type
            content = processed_file.get_content()
            mtype = processed_file.get_filetype()
            # Load it into the Patcher
            drpatch = Patcher(content, mtype)
            # Test if it can be patched
            test = drpatch.test()
            # Patch it. If it cannot be patched, it will be skipped.
            patched = drpatch.patch()
            # Update the content
            processed_file.update_content(patched)
            # Save it into a file
            processed_file.save_content()
        elif processed_path == None:
            print('The whole file list has been processed!')
            break
        else:
            pass

if __name__ == "__main__":
    main()

