#!/usr/bin/python3

"""
This script is a simple currency convertor.

Usage:
eurczk.py <FROM> <TO> <AMOUNT>

If no currencies are passed, we'll use EUR to CZK as default.
If no amount is passed, a conversion table will be printed.

The current exchange rates are taken automatically, i.e. you
do not have to bother with looking for them, but you need to
have the 'forex-python' module installed for the program to work.
It is available from pip.

Created by Lukas Ruzicka (lukas.ruzicka@gmail.com), 2023.
"""

import sys
from forex_python.converter import CurrencyRates, RatesNotAvailableError

rates = CurrencyRates()

def convert(rate, amount):
    """ Calculates the target amount using rate and source amount. """
    total = rate * amount
    return int(total)


# If sys.argv has one item, it means that there are no arguments
# provided. In that case, we will use EUR and CZK as currencies
# and we will print out a conversion table going from 1000 to 30000.
if len(sys.argv) == 1:
    rate = rates.get_rate('EUR', 'CZK')
    for i in range(1000, 31000, 1000):
        t = rate * i
        t = round(t, 2)
        print(f"EUR {i} => CZK {t}")

# If sys.argv has two items, it means that only one argument was given,
# probably the amount. In that case, we will use EUR and CZK as currencies
# and we will print out the target amount.
elif len(sys.argv) == 2:
    amount = sys.argv[1]
    rate = rates.get_rate('EUR', 'CZK')
    try:
        t = rate * float(amount)
    except ValueError:
        print("I expected an amount but received something that was not a number.")
        exit(1)

    t = round(t, 2)
    print(f"EUR {amount} => CZK {t}")

# If sys.argv has three items, it means that currencies were given, but no
# amount. In that case, we will use the currencies to calculate and print out
# the conversion table from 1000 to 30000.
elif len(sys.argv) == 3:
    cfrom = sys.argv[1].upper()
    cto = sys.argv[2].upper()
    try:
        rate = rates.get_rate(cfrom, cto)
    except RatesNotAvailableError:
        print("The rates were incorrectly set. Check the rate codes and try again.")
        exit(1)
    for i in range(1000, 31000, 1000):
        t = rate * i
        t = round(t, 2)
        print(f"{cfrom} {i} => {cto} {t}")

# If sys.argv has four items, it means that currencies were given, as well
# as the amount. In that case, we will use the currencies to calculate and print out
# the target amount using the source amount.
elif len(sys.argv) == 4:
    cfrom = sys.argv[1].upper()
    cto = sys.argv[2].upper()
    amount = sys.argv[3]
    rate = rates.get_rate(cfrom, cto)
    t = rate * float(amount)
    t = round(t, 2)
    print(f"{cfrom} {amount} => {cto} {t}")
# If anything else happens, we just warn and do nothing.
else:
    print("Incorrect number of arguments.")
    print("Usage: converter.py <from_currency> <to_currency> <amount>.")

