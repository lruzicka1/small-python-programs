#!/usr/bin/python

# This is a StepResize script which uses the imagemagick to perform slow resizes
# using the 5% steps for better results. It might also try to autosharpen the image
# after it has been resized.
#
# Usage: stepresize <filename> <target-size> [sharpen]
#
# If "sharpen" is left out, the image will not be sharpened.
#
#
# Created by Lukas Ruzicka <lukas.ruzicka@gmail.com> in 2022.
#
# Created under the GPL, v3 license.



import sys
import subprocess

# Read arguments
args = sys.argv
source = sys.argv[1]
target = int(sys.argv[2])
try:
    sharpen = sys.argv[3]
except KeyError:
    sharpen = None

# Get current size from the selected image.
# Get the imagemagick informative string about the image.
fileinfo = subprocess.run(["identify", source], capture_output=True)
# Convert the output into string and split it on spaces.
fileinfo = fileinfo.stdout.decode().split(' ')
# Read the size couplet from index 2.
cursize = fileinfo[2]
# Get both sides' sizes of the image.
side_a, side_b = cursize.split('x')

# Compare both sides and get the longer one.
# Assume side_a is longer.
side = int(side_a)
# If not, then take side_b
if side_a < side_b:
    side = int(side_b)

# Info
print(f"File {source} will be step-resized from {side} to {target} on the longer side.\n")

# Step increase using steps of 5% of the size.
if target < side:
    print("The image is already bigger than the selected size. Skipping.")
else:
    while True:
        subprocess.run(["convert", "-resize", "105%", source, source])
        side = int(side*1.05)
        print(f"The resizing has progressed to {side}.")
        if side >= target:
            break

# Try autosharpening.
if sharpen:
    ratio = int(target/800)
    print(f"Using {ratio}x{ratio} to sharpen the image.")
    subprocess.run(["convert", "-unsharp", f"{ratio}x{ratio}", source, source])

print("Finished correctly.")

