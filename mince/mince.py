#!/usr/bin/python

import re
import requests
import sys
from bs4 import BeautifulSoup

DATA = []

# Načti soubor s navštívenými daty
with open('visited.txt') as f:
    DATA = f.readlines()

# Odstřihni white spaces
DATA = [x.strip() for x in DATA]

try:
    akce = sys.argv[1]
    print(f"Akce: {akce}")
except KeyError:
    print("Nezadali jste URL stránky nebo soubor!")
    sys.exit(1)

def zkontroluj_stranku(url):
    # Načti stránku
    try:
        stranka = requests.get(url)
    except:
        return None
    # Projdi HTML parserem
    obsah = BeautifulSoup(stranka.content, "html.parser")
    # Najdi všechny obrázky
    obrazky = obsah.find_all("img")
    # Zjisti, jestli některý z nich ukazuje
    # na minci a pokud ano, napiš to.
    mince = None
    # Projdi nalezené obrázky
    for obrazek in obrazky:
        alt = obrazek.get("alt")
        # Jestliže tag existuje a je v něm slovo Megahru, zapamatuj si ho
        if alt and "Megahru" in alt:
            mince = url
    # Vrať jej
    return mince

def projdi_odkazy(url):
    # Načti stránku
    stranka = requests.get(url)
    # Projdi HTML parserem
    obsah = BeautifulSoup(stranka.content, "html.parser")
    # Najdi všechny odkazy
    odkazy = obsah.find_all("a")
    print(f"Prohledávám {len(odkazy)} odkazů.")
    # Pro každý odkaz
    for odkaz in odkazy:
        # Přečti tag href
        href = odkaz.get("href")
        # jestliže tam je celý odkaz, tak půjde hledání jenom směrem dolů
        if href and url in href:
            # podívej se, jestli už jsme ho neviděli a když ne
            if href not in DATA:
                # Projdi dotyčnou stránku
                mince = zkontroluj_stranku(href)
                # Jestli tam je mince, dej vědět.
                if mince:
                    print("        ", href)
                    uloz(href)

def uloz(link):
    link = f"{link}\n"
    with open('visited.txt', 'a') as f:
        f.write(link)

print("---------------------------------------------------------------------------")
print(f"V předchozích iteracích nalezeno {len(DATA)} mincí.")
print("Zadaný rozcestník vede k následujícím mincím:\n")

if akce == "file":
    try:
        soubor = sys.argv[2]
    except IndexError:
        print("Nezadali jste jméno souboru.")
        sys.exit(2)
else:
    URL = sys.argv[1]
    soubor = None

if soubor:
    with open(soubor) as linkdata:
        linky = linkdata.readlines()
    for link in linky:
        link = link.strip()
        print(f"\n------ Hledám mince v odkazu {link} ----- ")
        projdi_odkazy(link)
else:
    projdi_odkazy(URL)
