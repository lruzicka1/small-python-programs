#!/usr/bin/python3

import random

ALPHA = "abcdefghijklmnopqrstuvwxyz"
ALPHAC = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ .,:;-_?!@<>[]{}"

def split_text(text, size):
    """ Splits the message into chunks of size. """
    split = ""
    while text:
        chunk = text[0:size]
        split += f"{chunk} "
        text = text[size:]
    return split

def rearrange(alphabet, key):
    if key in alphabet:
        idx = alphabet.find(key)
    else:
        idx = int(key)
    first = alphabet[idx:]
    last = alphabet[:idx]
    new = first + last
    return new


def wordhide(message, word, decode=False):
    """ Wraps the message in words and splits accordingly. Use decode=True
    to decode a text that has been processed with this previously. """
    if decode == False:
        hidden = ""
        for letter in message:
            if letter == ' ':
                continue
            chunk = f"{word}{letter}"
            hidden += chunk
        encoded = hidden
    else:
        hidden = ""
        split = len(word)
        message = message.replace(" ","")
        where = message.find(word)
        while message:
            message = message[split:]
            try:
                letter = message[0]
            except IndexError:
                break
            message = message[1:]
            if letter != ' ':
                hidden += letter
        encoded = hidden
    return encoded

def reverse(message, split = 5, decode=False):
    """ Hides the message by reversing it and splitting it according to split. """
    message = message.replace(" ","")
    message = list(message)
    hidden = ""
    message.reverse()
    if decode == False:
        while message:
            chunk = message[0:split]
            message = message[split:]
            chunk = ''.join(chunk)
            hidden += f"{chunk} "
    else:
        hidden = ''.join(message)

    return hidden

def reveal_after_letter(message, letter, decode=False):
    """ The message characters will be shown after the given letter only.
    The distance between two letters is variable. """
    message = message.replace(" ", "")
    if decode == False:
        alphabet = list(ALPHA)
        count = random.randint(0,20)
        hidden = ""
        for m in message:
            for i in range(count):
                chunk = random.choice(alphabet)
                if chunk != letter:
                    hidden += chunk
            hidden += f"{letter}{m}"
    else:
        reveal = False
        hidden = ""
        for m in message:
            if m != letter and reveal == False:
                continue
            else:
                if reveal == False:
                    reveal = True
                else:
                    hidden += m
                    reveal = False

    return hidden

def repeat_every(message, interval, decode = False):
    """ The letters of the message will appear at a given interval. """
    message = message.replace(" ", "")
    hidden = ""
    if decode == False:
        alphabet = list(ALPHA)
        for m in message:
            for i in range(interval):
                filler = random.choice(alphabet)
                hidden += filler
            hidden += m
    else:
        count = 0
        for m in message:
            if count == interval:
                hidden += m
                count = 0
            else:
                count += 1

    return hidden

def swing(message, direction = 'normal', decode = False):
    """ The letters of the message will be taken interchangeably
    from the beginning and from the end of the strin. """
    message = message.replace(" ","")
    message = list(message)
    hidden = ""
    if decode == False:
        if direction != "normal":
            message.reverse()
        while message:
            hidden += message.pop(0)
            try:
                hidden += message.pop(-1)
            except IndexError:
                pass
    else:
        message.reverse()
        count = 0
        for m in message:
            if count == 0:
                hidden = f"{m}{hidden}"
                count += 1
            else:
                hidden = f"{hidden}{m}"
                count = 0
    return hidden

def move(message, key, alpha='complex', decode = False):
    """ Move the alphabet according to the key. The key could be a word or a sequence of numbers. Letter of the code will be converted to numbers. """
    hidden = ""
    if alpha == 'complex':
        alphabet = list(ALPHAC)
    else:
        alphabet = list(ALPHA)
    message = list(message)
    ratio = len(message) // len(key) + (len(message) % len(key) > 0)
    key = key * ratio
    print(f"zprava: {len(message)}, klice: {len(key)}")
    for k in key:
        if k in alphabet:
            i = alphabet.index(k)
            k = i
        try:
            m = message.pop(0)
        except IndexError:
            break
        new = rearrange(alphabet, k)
        if decode == False:
            position = new.index(m)
            em = alphabet[position]
            hidden += em
        else:
            position = alphabet.index(m)
            em = new[position]
            hidden += em
    return hidden


text = move('Kdyz jsem prisel vcera domu, videl jsem mrtvou zenu na koberci.', 'mrtvou', alpha='complex')
print(text)

text = move('o,54C-6:@v1][1._C1_:{VCZ2>1xC1[,._C-6:@v{]74>}C5![1v}WE@>W_]_?y', 'mrtvou', alpha='complex', decode=True)
print(text)



